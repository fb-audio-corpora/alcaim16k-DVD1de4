# Alcaim 16k DVD 1 de 4

Primeira parte do corpus CETUC. O corpus completo, por ser grande demais, foi
dividido em 4 repositórios:

- https://gitlab.com/fb-audio-corpora/alcaim16k-DVD1de4   
- https://gitlab.com/fb-audio-corpora/alcaim16k-DVD2de4   
- https://gitlab.com/fb-audio-corpora/alcaim16k-DVD3de4   
- https://gitlab.com/fb-audio-corpora/alcaim16k-DVD4de4   

# Descrição
Seção 4.1.3 da Dissertação de Mestrado de Rafael Oliveira (PPGCC, 2012):

> O Centro de Estudos em Telecomunicações (CETUC) [CETUC 2012], através do
Professor Doutor Abraham Alcaim, gentilmente cedeu ao LaPS, para fins de
pesquisa exclusivamente, seu corpus de áudio para Português Brasileiro. Esse
corpus, é composto por áudios de 1.000 sentenças, gravados por 101 locutores,
totalizando aproximadamente 143 horas de áudio.

__Grupo FalaBrasil (2019)__     
__Universidade Federal do Pará__       
